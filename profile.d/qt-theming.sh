# To use QGnomePlaform with AdwaitaQt style, comment out the following keys:

# Available options: gtk2, gnome, kde, qt5ct, xcb

# Note: Don't use in combination with QT_STYLE_OVERRIDE
export QT_QPA_PLATFORMTHEME="qt5ct"

# Available styles: HighContrastInverse, HighContrast, Adwaita-HighContrastInverse,
# Adwaita-HighContrast, Adwaita-Dark, Adwaita, kvantum-dark, kvantum, qt5ct-style,
# Windows, Fusion

# Note: Don't use in combination with QT_QPA_PLATFORMTHEME
#export QT_STYLE_OVERRIDE=""
